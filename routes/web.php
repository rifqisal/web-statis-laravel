<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::get('/welcome', 'AuthController@welcome');
Route::post('/welcome', 'AuthController@welcome_post');

route::get('/', function () {
    return view('welcome2');
});

route::get('/table', function () {
    return view('/table');
});

route::get('/data-tables', function () {
    return view('/data-tables');
});

// route::get('/cast', 'CastController@index');
// route::get('/cast/create', 'CastController@create');
// route::post('/cast', 'CastController@store');
// route::get('/cast/{cast_id}', 'CastController@show');
// route::get('/cast/{cast_id}/edit', 'CastController@edit');
// route::put('/cast/{cast_id}', 'CastController@update');
// route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::resource('cast', 'CastController');