@extends('layouts.master')

@section('title')
Edit Cast {{$cast->id}}
@endsection

@section('content')
<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name= "nama" value="{{$cast->nama}}" id="title" placeholder="Masukkan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Umur</label>
            <input type="number" class="form-control" name="umur" value="{{$cast->umur}}" id="body" placeholder="Masukkan umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Biodata</label>
            <input type="text" class="form-control" name="bio" value="{{$cast->bio}}" id="body" placeholder="Masukkan biodata">
            @error('biodata')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection