<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <div>
        <h2>Sign Up Form</h2>
        <form action='/welcome' method="post">
            @csrf
            <label for="nama_depan">First name:</label><br>
            <input type="text" id="nama_depan" name="nama_depan">
            <br><br>
            <label for="nama_belakang">Last name:</label><br>
            <input type="text" id="nama_belakang" name="nama_belakang"><br>
            <p>Gender:</p>
            <input type="radio" id="male" name="gender" value="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="other">Other</label><br><br>
            <label for="nationality">Nationality:</label><br><br>
            <select name="nationality" id="nationality">
                <option value="indo">Indonesian</option>
                <option value="cina">chinese</option>
                <option value="lebanon">lebanese</option>
                <option value="eropa">european</option>
            </select><br>
            <p>Language Spoken:</p>
            <input type="checkbox" id="bahasa" value="bahasa">
            <label for="bahasa">Bahasa Indonesia</label><br>
            <input type="checkbox" id="english" value="english">
            <label for="english">English</label><br>
            <input type="checkbox" id="other" value="other">
            <label for="other">Other</label><br><br>
            <label for="bio">Bio:</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
            <button>Sign Up</button>
        </form>
    </div>
</body>

</html>