<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;
use Symfony\Component\VarDumper\Caster\Caster;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // $query = DB::table('cast')->insert([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio']
        // ]);

        // nyimpen pake eloquent orm
        // $cast = new Cast;
        // $cast->nama = $request["nama"];
        // $cast->umur = $request["umur"];
        // $cast->bio = $request["bio"];
        // $cast->save();
        // eloquent orm

        //nyimpen pake Mass Assignment
        $cast = Cast::create([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        //nyimpen pake Mass Assignment

        return redirect('/cast');
    }

    public function index()
    {
        // $casts = DB::table('cast')->get();

        $casts = Cast::all(); //ini pake "Model" eloquent
        return view('cast.index', compact('casts'));
    }

    public function show($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id); //ini pake "Model" Eloquent
        return view('cast.show', compact('cast'));
    }

    public function edit($id)
    {
        // $cast = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id); //ini pake "Model" Eloquent
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // $query = DB::table('cast')
        //     ->where('id', $id)
        //     ->update([
        //         'nama' => $request['nama'],
        //         'umur' => $request['umur'],
        //         'bio' => $request['bio'],
        //     ]);

        //Update pake "Model" Eloquent
        $update = Cast::where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);
        //Update pake "Model" Eloquent

        return redirect('/cast');
    }

    public function destroy($id)
    {
        // $query = DB::table('cast')->where('id', $id)->delete();

        //Delete pake "Model" Eloquent
        Cast::destroy($id);
        //Delete pake "Model" Eloquent

        return redirect('/cast');
    }
}