<?php
//Eloquent ORM
namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "cast"; //Eloquent ORM

    protected $fillable = ["nama", "umur", "bio"]; //Mass Assignment
}